# LogAlerts

Program to collect and store alerts sent by Prometheus and display list of active and resolved alerts in a web page.

## Description
LogAlerts implements partially AlertManager v1 API used by Prometheus (and eventually others) to send alerts. The API is at /alerts/v1/alerts and is accepting only POST requests with JSON describing active alerts.
LogAlerts keeps track of active and resolved(not active, passed) alerts and stores them in SQLite3 database. The database is automatically created of startup if does not exists. The schema is hardcodded in the program.
It implements also simple web server to serve the single index page and the list of alerts in JSON format, which is then rendered to table by javascript.

## Functionality
There is single web page served, by default on port 9131. The page displays list of all alerts (active and resolved) started last 24 hours by default and optionally may be set to show active alerts only.  
Simple pagination is implemented to move back and forward by 24 hours. Also is possible to select manually day and hour. Active alerts are always displayed.  
Automatic refresh every 60 seconds is implemented and enable by default but it can be disabled.
Columns Name, Description and Query are clickable to display more extensive information of the alert.

## Bugs
Lack of connectivity, down time, misconfiguration may cause LogAlerts to miss the API message that an alert was resolved. Then the alert is listed always as active, even is not shown in Prometheus.  
The bug is mitigated by manually changing the "updatedAt" timestamp to be equal or
bigger by few seconds to the "endsAt" timestamp for the alerts where "updatedAt" is older than (2 hours in the examle) and endsAt is bigger than updatedAt. The following shell command may be used:
```sh
sqlite3 logalerts.db "update alerts set updatedAt=endsAt where endsAt>updatedAt and updatedAt<strftime('%s', 'now')-2*60*60;"
```
This functionality is now included in automatic housekeeping.

## Security
The API to update the alerts is allowed only from localhost, and so on the same host where prometheus is running. No other access control is implemented;  
Everyone who has access to the listening port and any IP of the machine can read the list of alerts;
Internal SQL queries are designed to be secure against common SQL threads (i.e. SQL injections).
## Installation
### prometheus
To collect alerts from prometheus it is necessary to include its local IP address and port in the “alerting” section of the prometheus configuration file:
```yaml
#alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - 127.0.0.1:9093 #AlertManager
      - 127.0.0.1:9193 #LogAlerts
```
Note: AlertManager(s) and LogAlerts work in parallel and independently of each-other;
### files
LogAlerts consists of single python file (logalerts.py) and 3 files with the webpage code (index.html, index.css and index.js). Those files are never served directly, but they are concatenated by the program and then sent to the client.   
The program requires python3 but no additional exotic modules are necessary.
### starting
It is managed by common OS startup manager like `systemd` or `supervisord` and may be run by restricted account. A part from sqlite3 database `logalerts.db` and it's journal `logalerts.db.wal` no other write access is needed.
### gateway
Access through a gateway server may be implemented with following section in the nginx server configuration:
```
location = /alerts/ {
  proxy_set_header X-Real-IP $remote_addr;
  proxy_set_header Host $host;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_pass http://10.10.10.10:9131/;
}
```
