var sttime;
var list;
var divs=[];
var targ;
var isbusy=false;
var fields=["status"];
var tsdiff; // server-browser time difference
var thetimer;
var cboxauto;
function refreshontimeout(){
  sttime.value=tstodatetime((new Date()/1000)-tsdiff);
  getdata();
  thetimer=setTimeout(refreshontimeout, 60000);
}
function changeauto(e){
  if (e.currentTarget.checked){
    sttime.value=tstodatetime((new Date()/1000)-tsdiff);
    getdata();
    thetimer=setTimeout(refreshontimeout, 60000);
  } else {
    clearTimeout(thetimer);
  }
}
function pager(e){
  chboxauto.checked=false;
  clearTimeout(thetimer);
  ts=(Math.trunc(Date.parse(sttime.value)/1000));
  if (e.currentTarget === pprev) ts-=86400;
  else if(e.currentTarget === pnext) ts+=86400;
  else alert('Bau!');
  sttime.value=tstodatetime(ts)
  getdata();
}
function tstodatetime(ts){
  var dt = new Date(ts*1000);
  var Y = dt.getFullYear();
  var M = "0"+(dt.getMonth()+1);
  var D = "0"+dt.getDate();
  var h = "0"+dt.getHours();
  var m = "0"+dt.getMinutes();
  var s = "0"+dt.getSeconds();
  return Y+"-"+M.substr(-2)+"-"+D.substr(-2)+" "+h.substr(-2)+ ':' + m.substr(-2);// + ':' + s.substr(-2);
}
function secondstotime(sec){
  if (sec/86400>=14) return Math.floor(sec/604800)+" weeks";
  else if (sec/3600>=48) return Math.floor(sec/86400)+" days";
  else if (sec/60>=120) return Math.floor(sec/3600)+" hours";
  else if (sec/60>=2) return Math.floor(sec/60)+" minutes";
  else return sec+" seconds"
}
function getservertimestamp(){
  var res;
  var request = new XMLHttpRequest();
  request.open('GET', '?type=timestamp', false);  // `false` makes the request synchronous
  request.send(null);
  if (request.status === 200) res=parseFloat(request.responseText)
  else res=(new Date())/1000;
  return res
}
document.addEventListener("DOMContentLoaded", function(event) {
  list=document.getElementById("list");
  sttime=document.getElementById("sttime");
  targ=document.getElementById("target");
  chboxauto=document.getElementById("auto");
  pnext=document.getElementById("pnext");
  pprev=document.getElementById("pprev");
  ts=(new Date()/1000);
  tsdiff=ts-getservertimestamp();
  sttime.value=tstodatetime(ts-tsdiff) //.toString()//.slice(0, 19).replace("T", " ");
  for (var i=0;i<fields.length;i++){
    divs[fields[i]]=document.getElementById(fields[i]);
  }
  sttime.addEventListener("blur",getdata,true);
  list.addEventListener("change",getdata,true);
  getdata();
  thetimer=setTimeout(refreshontimeout, 60000);
  chboxauto.addEventListener("change",changeauto,true);
  pnext.addEventListener("click",pager,true);
  pprev.addEventListener("click",pager,true);
  
});
function replaceTarg(a){
  targ.parentNode.replaceChild(a,targ);
  targ=a;
  //alert(targ.parentNode);
}
function setDivText(n,v){
  var t=document.createElement("div");
  t.appendChild(document.createTextNode(v));
  divs[n].parentNode.replaceChild(t,divs[n]);
  divs[n]=t
}
function setStatus(x){
  setDivText("status",x)
}
function destroydiv(e){
  e.currentTarget.parentNode.removeChild(e.currentTarget);
  e.stopPropagation();
}
function showquerydiv(e){
  d=document.createElement('div');
  rect = e.currentTarget.getBoundingClientRect();
  d.style.top=(e.clientY+window.scrollY-10)+'px';
  d.style.left=(e.clientX+window.scrollX-10)+'px';
  d.classList.add("float");
  d.classList.add("cless");
  t=d.appendChild(document.createElement('table'));
  //t.classList.add("cont");
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  tr.classList.add('centered');
  if (typeof e.currentTarget.dataHeadText !== 'undefined')td.appendChild(document.createTextNode(e.currentTarget.dataHeadText));
  if (e.currentTarget.dataIsActive){
    tr.classList.add('undelineactive');
  } else {
    tr.classList.add('undelineresolved');
  }
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  td.appendChild(document.createTextNode('Query:'));
  td.classList.add('bold');
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  td.appendChild(document.createTextNode(e.currentTarget.dataQuery));
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  td.appendChild(document.createTextNode('Link:'));
  td.classList.add('bold');
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  a=td.appendChild(document.createElement('a'));
  a.appendChild(document.createTextNode(e.currentTarget.dataQuery.substring(0,40)));
  a.href=e.currentTarget.dataLink;
  a.target='_blank';
  //document.body.appendChild(d);
  e.currentTarget.appendChild(d);
  d.addEventListener("click",destroydiv,false);
}
function showdictindiv(e){
  d=document.createElement('div');
  dict=e.currentTarget.dataLabels;
  rect = e.currentTarget.getBoundingClientRect();
  d.style.top=(e.clientY+window.scrollY-10)+'px';
  d.style.left=(e.clientX+window.scrollX-10)+'px';
  d.classList.add("float");
  d.classList.add("cless");
  t=d.appendChild(document.createElement('table'));
  //t.classList.add("cont");
  tr=t.appendChild(document.createElement('tr'))
  td=tr.appendChild(document.createElement('td'));
  tr.classList.add('centered');
  td.colSpan=2;
  if (typeof e.currentTarget.dataHeadText !== 'undefined')td.appendChild(document.createTextNode(e.currentTarget.dataHeadText));
  if (e.currentTarget.dataIsActive){
    tr.classList.add('undelineactive');
  } else {
    tr.classList.add('undelineresolved');
  }
  for (var key in dict){
    tr=t.appendChild(document.createElement('tr'));
    tr.classList.add('reset');
    td=tr.appendChild(document.createElement('td'));
    td.appendChild(document.createTextNode(key));
    td.classList.add('bold');
    td=tr.appendChild(document.createElement('td'));
    td.appendChild(document.createTextNode(dict[key]));
  }
  //document.body.appendChild(d);
  e.currentTarget.appendChild(d);
  d.addEventListener("click",destroydiv,false);
}
function createtable(data){
  var table = document.createElement("table");
  table.classList.add("cont");
  var columns, columnsnum;
  var tr = table.appendChild(document.createElement("tr"));
  //for (i=0;i<columnsnum;i++){
  //  tr.appendChild(document.createElement("th")).appendChild(document.createTextNode(columns[i]));
  //}
  for (var i = 0; i < data.length; i++) {
    if (0==i){
      columns=Object.keys(data[0]);
      columns=['Started At','Resolved At', 'Name', 'Description','Query']
      columnsnum=columns.length;
      for (var k=0;k<columnsnum;k++){
        tr.appendChild(document.createElement("th")).appendChild(document.createTextNode(columns[k]));
      }
    }
    var tr = table.appendChild(document.createElement("tr"));
    var td=tr.appendChild(document.createElement("td"));
    td.appendChild(document.createTextNode(tstodatetime(parseInt(data[i]['startsAt']))));
    if (data[i]['resolvedAt']==="") {
      tr.appendChild(document.createElement("td"));
      tr.classList.add("active");
    }else {
      tr.appendChild(document.createElement("td")).appendChild(document.createTextNode(tstodatetime(parseInt(data[i]['resolvedAt']))));
      tr.classList.add("resolved");
    }
    //alertname
    td=tr.appendChild(document.createElement("td"))
    td.appendChild(document.createTextNode(data[i]['alertName']));
    td.addEventListener("click",showdictindiv,false);
    td.dataLabels=data[i]['labels'];
    if (data[i]['resolvedAt']===""){
      td.dataIsActive=true;
      td.dataHeadText="Active for "+secondstotime((new Date()/1000)-tsdiff-parseInt(data[i]['startsAt']));
    }else{
      td.dataIsActive=false;
      td.dataHeadText="Resolved after "+secondstotime(parseInt(data[i]['resolvedAt'])-parseInt(data[i]['startsAt']));
    }
    td.classList.add("cmore");
    //annotations
    td=tr.appendChild(document.createElement("td"));
    td.addEventListener("click",showdictindiv,false);
    td.dataLabels=data[i]['annotations'];
    if (data[i]['resolvedAt']===""){
      td.dataIsActive=true;
      td.dataHeadText="Active for "+secondstotime((new Date()/1000)-tsdiff-parseInt(data[i]['startsAt']));
    }else{
      td.dataIsActive=false;
      td.dataHeadText="Resolved after "+secondstotime(parseInt(data[i]['resolvedAt'])-parseInt(data[i]['startsAt']));
    }
    td.classList.add("cmore");
    td.appendChild(document.createElement('div')).appendChild(document.createElement('strong')).appendChild(document.createTextNode(data[i]['summary']));
    td.appendChild(document.createElement('div')).appendChild(document.createTextNode(data[i]['description']));
    //
    td=tr.appendChild(document.createElement("td"));
    div=td.appendChild(document.createElement('div'));
    div.appendChild(document.createTextNode(data[i]['query']));
    div.classList.add("maxwidth");
    td.dataQuery=data[i]['query'];
    td.dataLink=data[i]['generatorUrl'];
    if (data[i]['resolvedAt']===""){
      td.dataIsActive=true;
      td.dataHeadText="Active for "+secondstotime((new Date()/1000)-tsdiff-parseInt(data[i]['startsAt']));
    }else{
      td.dataIsActive=false;
      td.dataHeadText="Resolved after "+secondstotime(parseInt(data[i]['resolvedAt'])-parseInt(data[i]['startsAt']));
    }
    td.addEventListener("click",showquerydiv,false);
    td.classList.add("cmore");

//    for (k = 0; k < columnsnum; k++) {
//      var cellValue = data[i][columns[k]];
//      if (cellValue == null) tr.appendChild(document.createElement("td"));
//      else {
//        var td=tr.appendChild(document.createElement("td"));
//        td.appendChild(document.createTextNode(cellValue));
//        //var arr=cellValue.split(/\r?\n/);
//        //for (var a=0;a<arr.length;a++){
//        //  if(a>0)td.appendChild(document.createElement("br"));
//        //  td.appendChild(document.createTextNode(arr[a]));
//        //}
//        //td.style.whiteSpace="nowrap"
//      }
//    }
  }
  replaceTarg(table)
}
function getdata(){
 if (!isbusy){
  isbusy=true;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
   if (this.readyState == 4) {
     if (this.status == 200) {
       var data = JSON.parse(this.responseText);
       createtable(data);
       if (data.length>0)setStatus("☺");
       else setStatus("∅");
     } else setStatus("☹" +this.status);
   }
  }
  xmlhttp.open("GET", "?type=list&list="+list.value+"&sttime="+(Math.trunc(Date.parse(sttime.value)/1000)),true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  setStatus("◌")
  xmlhttp.send();
  isbusy=false;
 }
}
