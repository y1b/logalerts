#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging,logging.handlers
import json
import sqlite3
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from datetime import datetime
from time import time
import urllib.parse
import os
import zlib

def thelogger():
    return logging.getLogger('logalerts')

thelogger().setLevel(logging.INFO)
basepath=os.path.dirname(os.path.abspath(__file__))

cursor = sqlite3.connect(basepath+'/'+'logalerts.db',check_same_thread=False).cursor()
_=cursor.execute('PRAGMA foreign_keys=ON')
_=cursor.execute('PRAGMA temp_store =3')
_=cursor.execute('PRAGMA journal_mode=TRUNCATE')
_=cursor.executescript('''
CREATE TABLE IF NOT EXISTS status (
  statusName CHAR[8] PRIMARY KEY NOT NULL,
  statusInt INTEGER
);
CREATE TABLE IF NOT EXISTS labelNames (
  labelNameId UNSIGNED INTEGER PRIMARY KEY NOT NULL,
  labelName VARCHAR NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS labelValues (
  labelValueId UNSIGNED INTEGER PRIMARY KEY NOT NULL,
  labelValue VARCHAR NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS fingerprints (
  fingerprintId UNSIGNED INTEGER PRIMARY KEY NOT NULL,
  fingerprint CHAR[16] NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS alerts (
  startsAt UNSIGNED INTEGER NOT NULL,
  fingerprintId UNSIGNED INTEGER NOT NULL,
  endsAt UNSIGNED INTEGER DEFAULT NULL,
  generatorUrlId INTEGER DEFAULT NULL,
  updatedAt UNSIGNED INTEGER NOT NULL,
  PRIMARY KEY (startsAt, fingerprintId),
  FOREIGN KEY(fingerprintId) REFERENCES fingerprints(fingerprintId) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY(generatorUrlId) REFERENCES labelValues(labelValueId) ON DELETE RESTRICT ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS labels (
  fingerprintId UNSIGNED INTEGER NOT NULL,
  labelNameId UNSIGNED INTEGER NOT NULL,
  labelValueId UNSIGNED INTEGER NOT NULL,
  PRIMARY KEY (fingerprintId, labelNameId),
  FOREIGN KEY (fingerprintId) REFERENCES fingerprints(fingerprintId) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (labelNameId) REFERENCES labelNames(labelNameId) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (labelValueId) REFERENCES labelValues(labelValueId) ON DELETE RESTRICT ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS annotations (
  startsAt UNSIGNED INTEGER NOT NULL,
  fingerprintId UNSIGNED INTEGER NOT NULL,
  labelNameId UNSIGNED INTEGER NOT NULL,
  labelValueId UNSIGNED INTEGER NOT NULL,
  PRIMARY KEY (startsAt, fingerprintId, labelNameId),
  FOREIGN KEY (startsAt, fingerprintId) REFERENCES alerts(startsAt, fingerprintId) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (labelNameId) REFERENCES labelNames(labelNameId) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (labelValueId) REFERENCES labelValues(labelValueId) ON DELETE CASCADE ON UPDATE CASCADE
);
''')

def labelsToSignature(d):
  #re-implemnted fnv64a algorithm from https://github.com/prometheus/common/blob/886bed85d9dc60c52e5efa54441a5e278a35e2ab/model/signature.go#L100
  hval=0xcbf29ce484222325 #hval_init
  fnv_size = 2**64
  fnv_prime= 0x100000001b3
  if isinstance(d,dict):
    for key in sorted(d):
      for byte in key.encode('utf-8')+b'\xff'+d[key].encode('utf-8')+b'\xff':
        hval = hval ^ byte
        hval = (hval * fnv_prime) % fnv_size
  return hval

def jsontimeToTimestamp(s):
  utc_dt = datetime.strptime(s[:19]+'Z', '%Y-%m-%dT%H:%M:%SZ')
  timestamp = (utc_dt - datetime(1970, 1, 1)).total_seconds()
  return int(timestamp)

def timestampToDatetimeString(t):
  return datetime.fromtimestamp(t)

def putgetFingerprintId(c,s):
  if None!=s:
    q='SELECT fingerprintId FROM fingerprints WHERE fingerprint=:fingerprint LIMIT 1'
    p={'fingerprint':s}
    c.execute(q, p)
    rows=c.fetchall();
    if len(rows)>0:
      return rows[0][0]
    else:
      c.execute('INSERT INTO fingerprints (fingerprintId,fingerprint) SELECT CASE WHEN count(1) IS 0 THEN 1 ELSE max(fingerprintId)+1 END, :fingerprint FROM fingerprints',p)
      c.execute(q, p)
      return [row for row in c][0][0]
  else:
    return None    

def putgetLabelValueId(c,s):
  if None!=s:
    q='SELECT labelValueId FROM labelValues WHERE labelValue=:labelvalue LIMIT 1'
    p={'labelvalue':s}
    c.execute(q, p)
    rows=c.fetchall();
    if len(rows):
      return rows[0][0]
    else:
      c.execute('''INSERT INTO labelValues (labelValueId,labelValue) SELECT CASE WHEN count(1) IS 0 THEN 1 ELSE (SELECT t1.labelValueId+1 
FROM labelValues t1
WHERE NOT EXISTS(SELECT * FROM labelValues t2 WHERE t2.labelValueId = t1.labelValueId + 1)
ORDER BY t1.labelValueId LIMIT 1) END, :labelvalue FROM labelValues''',p)
      c.execute(q, p) #Insert the labelvalue and the lowesd unused integer as labelValueId
      return [row for row in c][0][0]
  else:
    return None    

def putgetLabelNameId(c,s):
  if None!=s:
    q='SELECT labelNameId FROM labelNames WHERE labelName=:labelname LIMIT 1'
    p={'labelname':s}
    c.execute(q, p)
    rows=c.fetchall();
    if len(rows):
      return rows[0][0]
    else:
      c.execute('INSERT INTO labelNames (labelNameId,labelName) SELECT CASE WHEN count(1) IS 0 THEN 1 ELSE max(labelNameId)+1 END, :labelname FROM labelNames',p)
      c.execute(q, p)
      return [row for row in c][0][0]
  else:
    return None    

def houseKeeping(c,ts):
  p={'stname':'HausKpTs','ts':ts}
  c.execute('SELECT statusInt FROM status WHERE statusName=:stname LIMIT 1',p)
  rows=c.fetchall();
  if len(rows)==0:
    c.execute('INSERT INTO status (statusName, statusInt) VALUES (:stname,:ts)',p)
  elif rows[0][0]+3600<ts:
    c.execute('''DELETE FROM labelValues where labelValueId not in ( 
select labelValueId from labels 
UNION
select labelValueId from annotations
UNION
select generatorUrlId from alerts
)''')
    if c.rowcount>0:
        thelogger().info('Housekeeping: Deleted %d unused strings'%c.rowcount)
    c.execute('''update alerts set updatedAt=endsAt 
where endsAt>updatedAt and updatedAt<strftime('%s', 'now')-2*60*60
''')
    if c.rowcount>0:
        thelogger().info('Housekeeping: autoresolved %d stale alerts'%c.rowcount)
    c.execute('UPDATE status SET statusInt=:ts WHERE statusName=:stname',p)

def getpathparams(s):
    t=s.split('?',1)
    r={}
    if len(t)>1:
      r = {x[0] if len(x)>1 else '' : x[1] if len(x)>1 else x[0] for x in [x.split("=",1) for x in t[1].split("&") ]}
    return (t[0],r)

#putupdateAlert(cursor,startsAt,fpid,endsAt,purlid,ts)
def putupdateAlert(c,startts,fpid,endts,genurlid,ts):
  '''returns True if new alert is added of False if updated already existing'''
  if startts is not None and fpid is not None:
    q='SELECT endsAt,generatorUrlId FROM alerts WHERE startsAt=:startts AND fingerprintId=:fpid LIMIT 1'
    p={'startts':startts,'fpid':fpid,'ts':ts}
    c.execute(q,p)
    rows=c.fetchall();
    if len(rows):
        sets=['updatedAt=:ts']
        if endts is not None:
          sets.append('endsAt=:endts')
          p.update({'endts':endts})
        if genurlid != rows[0][1]:
          sets.append('generatorUrlId=:genurlid')
          p.update({'genurlid':genurlid})
        t='UPDATE alerts SET %s WHERE startsAt=:startts AND fingerprintId=:fpid'%','.join(sets)
        c.execute(t,p)
        isNew=False
    else:
        cols=['startsAt','fingerprintId','updatedAt']
        vals=[':startts',':fpid',':ts']
        if endts is not None:
            cols.append('endsAt')
            vals.append(':endts')
            p.update({'endts':endts})
        if genurlid is not None:
            cols.append('generatorUrlId')
            vals.append(':genurlid')
            p.update({'genurlid':genurlid})
        c.execute('INSERT INTO alerts (%s) VALUES (%s)'%(','.join(cols),','.join(vals)),p)
        isNew=True
    return isNew
        
def putlabels(c,fpid,d):
  if fpid is not None and d is not None:
    c.execute('SELECT 1 FROM labels WHERE fingerprintId=:fpid limit 1',{'fpid':fpid})
    rows=c.fetchall();
    if len(rows)==0:
      q='INSERT INTO labels (fingerprintId,labelNameId,labelValueId) values (:fpid,:nid,:vid)'
      for k in d:
        nid=putgetLabelNameId(c,k)
        vid=putgetLabelValueId(c,d[k])
        c.execute(q,{'fpid':fpid,'nid':nid,'vid':vid})

def putupdannotations(c,startts,fpid,d):
  if startts is not None and fpid is not None and d is not None:
    cursor.execute('SELECT labelNameId,labelValueId FROM annotations WHERE startsAt=:startts and fingerprintId=:fpid',{'fpid':fpid, 'startts':startts})
    rows=c.fetchall();
    new=[(putgetLabelNameId(c,k),putgetLabelValueId(c,d[k])) for k in d]
    for t in list(set(rows)-set(new)):
        cursor.execute('DELETE FROM annotations WHERE startsAt=:startts and fingerprintId=:fpid and labelNameId=:nid',{'fpid':fpid, 'startts':startts, 'nid':t[0]})
    for t in list(set(new)-set(rows)):
        cursor.execute('INSERT INTO annotations (startsAt,fingerprintId,labelNameId,labelValueId) values (:startts,:fpid,:nid,:vid)',{'startts':startts,'fpid':fpid,'nid':t[0],'vid':t[1]})

def getAlertLabels(c,fp):
  if None!=fp:
    q='''select labelName,labelValue from labels
join labelNames on labels.labelNameId=labelNames.labelNameId
join labelValues on labels.labelValueId=labelValues.labelValueId
join fingerprints on labels.fingerprintId=fingerprints.fingerprintId
where fingerprint=:fingerprint
order by labelName;'''
    p={'fingerprint':fp}
    c.execute(q, p)
    return{x[0]:x[1]for x in c}
  else:
    return {}    
def getAlertAnnotations(c,fp,startts):
  if fp is not None and startts is not None :
    q='''select labelName,labelValue from annotations
join labelNames on annotations.labelNameId=labelNames.labelNameId
join labelValues on annotations.labelValueId=labelValues.labelValueId
join fingerprints on annotations.fingerprintId=fingerprints.fingerprintId
where fingerprint=:fingerprint and startsAt=:startts
order by labelName;'''
    p={'fingerprint':fp,'startts':startts}
    c.execute(q, p)
    return{x[0]:x[1]for x in c}
  else:
    return {}    


qgetalerts='''SELECT 
alerts.startsAt,
CASE WHEN alerts.endsAt>alerts.updatedAt THEN "" ELSE alerts.endsAt END resolvedAt,
fingerprint,
a.labelValue as alertName,
b.labelValue as summary,
c.labelValue as description,
d.labelValue as generatorUrl
from alerts
left join fingerprints on alerts.fingerprintId=fingerprints.fingerprintId
left join ( select fingerprintId, labelValue from labels
  join labelNames on labels.labelNameId=labelNames.labelNameId and labelNames.labelName='alertname'
  join labelValues on labels.labelValueId=labelValues.labelValueId
) a on alerts.fingerprintId=a.fingerprintId
left join ( select startsAt,fingerprintId, labelValue from annotations
  join labelNames on annotations.labelNameId=labelNames.labelNameId and labelNames.labelName='summary'
  join labelValues on annotations.labelValueId=labelValues.labelValueId
) b on alerts.fingerprintId=b.fingerprintId and alerts.startsAt=b.startsAt
left join ( select startsAt,fingerprintId, labelValue from annotations
  join labelNames on annotations.labelNameId=labelNames.labelNameId and labelNames.labelName='description'
  join labelValues on annotations.labelValueId=labelValues.labelValueId
) c on alerts.fingerprintId=c.fingerprintId and alerts.startsAt=c.startsAt
left join labelValues d on alerts.generatorUrlId=d.labelValueId
'''

#print("%016x"%labelsToSignature(t1.get('labels')))
#exit(0)
class S(BaseHTTPRequestHandler):
    server_version= "AlertList/0/9"
    
    def log_message(self, format, *args):
        thelogger().debug("%s - - [%s] %s\n" % (self.address_string(),self.log_date_time_string(),format%args))

    def sendresult(self,status=200,message="Ok",contenttype='text/html',content=''):
        resultbytes=content.encode('utf-8');
        self.send_response(status,message)
        self.send_header('Content-type', contenttype)
        self.send_header('Expires','Thu, 01 Jan 1970 00:00:00 GMT') #don't even think on caching
        if len(content) > 100:                       # don't bother compressing small data
          if 'accept-encoding' in self.headers:    # case insensitive
            if 'gzip' in self.headers['accept-encoding']:
                gzip_compress = zlib.compressobj(6, zlib.DEFLATED, zlib.MAX_WBITS | 16)
                resultbytes = gzip_compress.compress(resultbytes) + gzip_compress.flush()
                del gzip_compress
                self.send_header('content-encoding', 'gzip')
        self.send_header('Content-length', '%d'%len(resultbytes))
        self.end_headers()
        self.wfile.write(resultbytes)

    def do_GET(self):
        path,params=getpathparams(self.path)
        #print(path,params)
        if path == '/':
          if len(params)==0:
            with open(basepath+'/'+'index.html',encoding='utf-8') as f:
                indexhtml=f.read()
            with open(basepath+'/'+'index.css',encoding='utf-8') as f:
                indexcss=f.read()
            with open(basepath+'/'+'index.js',encoding='utf-8') as f:
                indexjs=f.read()
            self.sendresult(content=indexhtml.format(css=indexcss,javascript=indexjs))
          elif params.get('type')=='timestamp':
              self.sendresult(content='%f'%(time()))
          elif params.get('type')=='list':
              q=qgetalerts+" WHERE "
              if params.get('list')=='all':
                  q+="alerts.startsAt between :ts-(24*60*60) and :ts or "
              q+="alerts.endsAt> alerts.updatedAt "
              q+="order by alerts.startsAt desc"
              cursor.execute(q,{'ts':params.get('sttime')})
              res=[]
              for row in cursor:
                  pp,pq=getpathparams(row[6])
                  resline={
                      'startsAt':row[0],
                      'resolvedAt':row[1],
                      'fingerprint':row[2],
                      'alertName':row[3],
                      'summary':row[4],
                      'description':'' if row[5] is None else row[5],
                      'generatorUrl':row[6],
                      'query':'' if pq.get('g0.expr') is None else urllib.parse.unquote_plus(pq['g0.expr']),
                      #
                  }
                  res.append(resline)
              for al in res:
                  al.update({'labels':getAlertLabels(cursor,al.get('fingerprint'))})
              for al in res:
                  al.update({'annotations':getAlertAnnotations(cursor,al.get('fingerprint'),al.get('startsAt'))})
              s=json.dumps(res)
              self.sendresult(content=s,contenttype='application/json')
          else:
              self.sendresult()
        else:
            self.sendresult(404,'Not Found')

    def do_POST(self):
        ts=int(time())
        houseKeeping(cursor,ts)
        content_length = int(self.headers['Content-Length']) if self.headers['Content-Length'] is not None else 0 # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        #logging.info("POST: Path: %s BodyLength: %d",str(self.path), len(post_data))
        if self.path=='/api/v1/alerts':
          if self.client_address[0].startswith('127.'):
            try:
                alerts=json.loads(post_data.decode('utf-8'))
            except Exception as e:
                thelogger().error("json invalid",e)
                self.sendresult(411,'Invalid JSON')
                return
            s=''
            for a in alerts:
                labels=a.get('labels')
                fingerprint=labelsToSignature(labels)
                startsAt=jsontimeToTimestamp(a.get('startsAt'))
                endsAt=jsontimeToTimestamp(a.get('endsAt'))
                generatorURL=a.get('generatorURL')
                fpid=putgetFingerprintId(cursor,'%016x'%fingerprint)
                purlid=putgetLabelValueId(cursor,generatorURL)
                putupdateAlert(cursor,startsAt,fpid,endsAt,purlid,ts)
                putlabels(cursor,fpid,labels)
                putupdannotations(cursor,startsAt,fpid,a.get('annotations'))
                cursor.connection.commit()
            self.sendresult(content=s)
          else:
            thelogger().error("not allowed %s"%self.client_address[0])
            self.sendresult(403,'Localhost only allowed')
        else:
            self.sendresult(404,'Not Found')

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""
    pass
            
def run(server_class=ThreadedHTTPServer, handler_class=S, port=9193):
    sysloghandler=logging.handlers.SysLogHandler(facility=logging.handlers.SysLogHandler.LOG_DAEMON,address='/dev/log')
    sysloghandler.setFormatter(logging.Formatter(fmt="%(name)s:[%(process)s] %(funcName)s:%(lineno)d '%(message)s'"))
    thelogger().addHandler(sysloghandler) 
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    thelogger().info('Starting httpd in %s...'%basepath)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    thelogger().info('Stopping httpd...')

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()

cursor.connection.close()
